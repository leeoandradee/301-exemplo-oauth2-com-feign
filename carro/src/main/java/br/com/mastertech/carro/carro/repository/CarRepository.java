package br.com.mastertech.carro.carro.repository;

import br.com.mastertech.carro.carro.models.Car;
import org.springframework.data.repository.CrudRepository;

public interface CarRepository extends CrudRepository<Car, String> {
}
