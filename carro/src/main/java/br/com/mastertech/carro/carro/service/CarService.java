package br.com.mastertech.carro.carro.service;

import br.com.mastertech.carro.carro.CarNotFoundException;
import br.com.mastertech.carro.carro.models.Car;
import br.com.mastertech.carro.carro.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.websocket.server.ServerEndpoint;
import java.util.Optional;

@Service
public class CarService {

    @Autowired
    private CarRepository carRepository;

    public Car create(Car car) {
        return carRepository.save(car);
    }

    public Car getByPlate(String plate) {
        Optional<Car> byId = carRepository.findById(plate);

        if(!byId.isPresent()) {
            throw new CarNotFoundException();
        }

        return byId.get();
    }
}
