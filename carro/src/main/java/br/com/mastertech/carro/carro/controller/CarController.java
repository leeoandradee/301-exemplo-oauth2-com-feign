package br.com.mastertech.carro.carro.controller;

import br.com.mastertech.carro.carro.models.Car;
import br.com.mastertech.carro.carro.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
public class CarController {

    @Autowired
    private CarService carService;

    @PostMapping
    public Car create(@RequestBody Car car) {
        return carService.create(car);
    }

    @GetMapping("/{plate}")
    public Car getByPlate(@PathVariable String plate, Principal principal) {
        System.out.println(">>>>>>>>>>>>>>>>>>");
        System.out.println(principal.getName());
        System.out.println(">>>>>>>>>>>>>>>>>>");
        return carService.getByPlate(plate);
    }

}
