package br.com.mastertech.pessoa.pessoa.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CAR", configuration = CarClientConfiguration.class)
public interface CarClient {

    @GetMapping("/{plate}")
    Car getByPlate(@PathVariable String plate);

}
