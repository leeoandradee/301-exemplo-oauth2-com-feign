package br.com.mastertech.pessoa.pessoa.client;

public class Car {

    private String plate;

    private String model;

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
