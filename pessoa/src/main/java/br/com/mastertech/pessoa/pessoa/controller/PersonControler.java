package br.com.mastertech.pessoa.pessoa.controller;

import br.com.mastertech.pessoa.pessoa.client.Car;
import br.com.mastertech.pessoa.pessoa.client.CarClient;
import br.com.mastertech.pessoa.pessoa.client.Cep;
import br.com.mastertech.pessoa.pessoa.client.CepClient;
import br.com.mastertech.pessoa.pessoa.model.Person;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PersonControler {

    @Autowired
    private CarClient carClient;

    @Autowired
    private CepClient cepClient;

    @GetMapping("/{name}/{plate}")
    public Person create(@PathVariable String name, @PathVariable String plate) {
        Person person = new Person();
        person.setName(name);

        Car byPlate = carClient.getByPlate(plate);
        person.setCarModel(byPlate.getModel());

        return person;
    }

    @GetMapping("/semlogin/{name}/{plate}")
    public Person createSemLogin(@PathVariable String name, @PathVariable String plate) {
        Person person = new Person();
        person.setName(name);

        Car byPlate = carClient.getByPlate(plate);
        person.setCarModel(byPlate.getModel());

        return person;
    }

    @GetMapping("/{cep}")
    public Cep create(@PathVariable String cep) {
        return cepClient.getCep(cep);
    }
}
