package br.com.mastertech.pessoa.pessoa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Person {

    private String name;

    private String carModel;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }
}
